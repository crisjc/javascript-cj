entre number ![image](/uploads/459d4e641633847f087e522969ae14ba/image.png)

number indefined ![image](/uploads/851cb631bd066b9b3f184ffe97cbd32e/image.png)

number null ![image](/uploads/e7380e7811611609d1845c26ee8e6903/image.png)

number string ![image](/uploads/48bb3364d909e9589a1271a1c76ede99/image.png)

number y booelan ![image](/uploads/9fd8b70561bce4b817540467d09a0d0f/image.png)

indefinidos ![image](/uploads/851cb631bd066b9b3f184ffe97cbd32e/image.png)

indefinidos y string ![image](/uploads/c16ee046b86fc6b19286105cb77180a8/image.png)

indefinido y null ![image](/uploads/851cb631bd066b9b3f184ffe97cbd32e/image.png)

indefinido y boolean ![image](/uploads/851cb631bd066b9b3f184ffe97cbd32e/image.png)

entre dos valores string ![image](/uploads/2e97f72c12f24977d3da093b26d685df/image.png)

entre string y null ![image](/uploads/a160fdc003e13ca6e6cd3fe13ae37b88/image.png)

string y booelan ![image](/uploads/1060b08bff1de16d4c14fe5b90cb74be/image.png) 

entre dos valores nulos ![image](/uploads/a43a2dcab8dbb0db958bbe8449d4b859/image.png)

nulo y booelan ![image](/uploads/b12a0f0ecaa8a2718f61323b1743991d/image.png)

dos valores boolean ![image](/uploads/27b4ba2f507aaec94908531de1231691/image.png)

# Operaciones Aritméticas

Reallizar las diferentes operaciones aritmeticas de suma, resta, multiplicacion, divicion, modulo

1 Se creo un JSON y dentro se ingreso las 5 operaciones donde debera ingresar como parametros un valor a ya b.
~~~
var operacionesAritmeticas ={
    suma: (a,b)=>{
        return a+b
    },resta:(a,b)=>{
        return a-b
    },multiplicacion:(a,b)=>{
        return a*b
    },divicion:(a,b)=>{
        return a/b
    },modulo:(a,b)=>{
        return a%b
    },
}
~~~
2 Probando las diferentes opreaciones 

* operaciones entre dos valores number

**Entrada**
~~~
console.log(operacionesAritmeticas.suma(4,5))
console.log(operacionesAritmeticas.resta(4,5))
console.log(operacionesAritmeticas.multiplicacion(4,5))
console.log(operacionesAritmeticas.divicion(4,5))
console.log(operacionesAritmeticas.modulo(4,5)) 
~~~
**Salida**

![image](/uploads/459d4e641633847f087e522969ae14ba/image.png)

* operaciones entre un number y un undefined

**Entrada**
~~~
console.log(operacionesAritmeticas.suma(5))
console.log(operacionesAritmeticas.resta(5))
console.log(operacionesAritmeticas.multiplicacion(5))
console.log(operacionesAritmeticas.divicion(5))
console.log(operacionesAritmeticas.modulo(5))
~~~
**Salida**

* operaciones entre un number y un null

**Entrada**
~~~
console.log(operacionesAritmeticas.suma(5,null))
console.log(operacionesAritmeticas.resta(5,null))
console.log(operacionesAritmeticas.multiplicacion(5,null))
console.log(operacionesAritmeticas.divicion(5,null))
console.log(operacionesAritmeticas.modulo(5,null)) 
~~~
**Salida**

* operaciones entre un number y un string

**Entrada**
~~~
console.log(operacionesAritmeticas.suma(5,'a'))
console.log(operacionesAritmeticas.resta(5,'a'))
console.log(operacionesAritmeticas.multiplicacion(5,'a'))
console.log(operacionesAritmeticas.divicion(5,'a'))
console.log(operacionesAritmeticas.modulo(5,'a')) 
~~~
**Salida**



* operaciones entre un number y un boolean


**Entrada**
~~~
console.log(operacionesAritmeticas.suma(5,true))
console.log(operacionesAritmeticas.resta(5,false))
console.log(operacionesAritmeticas.multiplicacion(5,true))
console.log(operacionesAritmeticas.divicion(5,true))
console.log(operacionesAritmeticas.modulo(5,true))
~~~
**Salida**

* operaciones entre dos valores indefinidos

**Entrada**
~~~
console.log(operacionesAritmeticas.suma())
console.log(operacionesAritmeticas.resta())
console.log(operacionesAritmeticas.multiplicacion())
console.log(operacionesAritmeticas.divicion())
console.log(operacionesAritmeticas.modulo())
~~~

**Salida**

* operaciones entre un indefinido y un string

**Entrada**
~~~
console.log(operacionesAritmeticas.suma('hh'))
console.log(operacionesAritmeticas.resta('hh'))
console.log(operacionesAritmeticas.multiplicacion('hh'))
console.log(operacionesAritmeticas.divicion('hh'))
console.log(operacionesAritmeticas.modulo('hh'))
~~~
**Salida**

* operaciones entre un indefinido y un null

**Entrada**
~~~
console.log(operacionesAritmeticas.suma(null))
console.log(operacionesAritmeticas.resta(null))
console.log(operacionesAritmeticas.multiplicacion(null))
console.log(operacionesAritmeticas.divicion(null))
console.log(operacionesAritmeticas.modulo(null))
~~~
**Salida**

* operaciones entre un indefinido y boolean

**Entrada**
~~~
console.log(operacionesAritmeticas.suma(true))
console.log(operacionesAritmeticas.resta(false))
console.log(operacionesAritmeticas.multiplicacion(true))
console.log(operacionesAritmeticas.divicion(true))
console.log(operacionesAritmeticas.modulo(true))
~~~
**Salida**

* operaciones entre dos valores string

**Entrada**
~~~
console.log(operacionesAritmeticas.suma('b','a'))
console.log(operacionesAritmeticas.resta('2','3'))
console.log(operacionesAritmeticas.multiplicacion('4','a'))
console.log(operacionesAritmeticas.divicion('b','a'))
console.log(operacionesAritmeticas.modulo('b','a'))
~~~

**Salida**

* operaciones entre un valor string y null

**Entrada**
~~~
console.log(operacionesAritmeticas.suma('b',null))
console.log(operacionesAritmeticas.resta('2',null))
console.log(operacionesAritmeticas.multiplicacion('4',null))
console.log(operacionesAritmeticas.divicion('b',null))
console.log(operacionesAritmeticas.modulo('b',null))
~~~

**Salida**

* operaciones entre un valor string y boolean

**Entrada**
~~~
console.log(operacionesAritmeticas.suma('b',true))
console.log(operacionesAritmeticas.resta('2',true))
console.log(operacionesAritmeticas.multiplicacion('4',true))
console.log(operacionesAritmeticas.divicion('b',true))
console.log(operacionesAritmeticas.modulo('b',true))
~~~

**Salida**

* operaciones entre dos valores nulos 

**Entrada**

~~~
console.log(operacionesAritmeticas.suma(null,null))
console.log(operacionesAritmeticas.resta(null,null))
console.log(operacionesAritmeticas.multiplicacion(null,null))
console.log(operacionesAritmeticas.divicion(null,null))
console.log(operacionesAritmeticas.modulo(null,null))
~~~

**Salida**

* operaciones entre un valor nulo y boolean

**Entrada**
~~~
console.log(operacionesAritmeticas.suma(null,true))
console.log(operacionesAritmeticas.resta(null,true))
console.log(operacionesAritmeticas.multiplicacion(null,true))
console.log(operacionesAritmeticas.divicion(null,true))
console.log(operacionesAritmeticas.modulo(null,true))
~~~

**Salida**

* operaciones entre dos valores del tipo boolean

**Entrada**
~~~
console.log(operacionesAritmeticas.suma(true,true))
console.log(operacionesAritmeticas.resta(false,true))
console.log(operacionesAritmeticas.multiplicacion(true,true))
console.log(operacionesAritmeticas.divicion(true,true))
console.log(operacionesAritmeticas.modulo(true,true))
~~~

**Salida**
