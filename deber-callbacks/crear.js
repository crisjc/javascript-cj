// crear usuario
module.exports = (arregloUsuarios, objetoNombreApellido, cb) => {
    arregloUsuarios.forEach((valor, indice) => {
        let condicionBusqueda = valor.nombre === objetoNombreApellido.nombre && valor.apellido === objetoNombreApellido.apellido
        if (condicionBusqueda) {
            cb({
                info: `el usuario ${valor.nombre} ${valor.apellido} ya existe}`
            })
        }
    });
    cb({
        detalle: 'fue creado el usuario',
        indice:arregloUsuarios.push(objetoNombreApellido),
        info: arregloUsuarios[10]
    })
}