// eliminar usuario 
module.exports = (arregloUsuarios, objetoNombreApellido,cb)=>{
    arregloUsuarios.forEach((valor, indice) => {
        let condicionBusqueda = valor.nombre === objetoNombreApellido.nombre && valor.apellido === objetoNombreApellido.apellido
        if (condicionBusqueda) {
            arregloUsuarios = arregloUsuarios.splice(indice)
            cb({
            eliminado:`el usuario ${objetoNombreApellido.nombre} ${objetoNombreApellido.apellido} fue eliminado}`
            })
        }
    });
}
