const arregloUsuario = require('./arreglo-usuarios.js')
const crear = require('./crear.js')
const buscar = require('./buscar.js')
const buscarCrear = require('./buscar-crear.js')
const eliminarUsuario = require('./elimianar-usuario')

module.exports = {
    arregloUsuario,
    crear,
    buscar,
    buscarCrear,
    eliminarUsuario
}