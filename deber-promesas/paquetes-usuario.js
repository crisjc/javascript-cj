const arregloUsuario = require('./arreglo-usuarios.js')
const crear = require('./crear.js')
const buscar = require('./buscar.js')
const buscarCrear = require('./buscar-crear.js')
const eliminarUsuario = require('./elimianar-usuario')
const objetoUsuarioCrearBuscar = require('./arreglo-usuarios')

module.exports = {
    arregloUsuario,
    crear,
    buscar,
    buscarCrear,
    eliminarUsuario,
    objetoUsuarioCrearBuscar
}