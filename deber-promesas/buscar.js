// buscar usuario
module.exports = (arregloUsuarios, objetoNombreApellido) => {
    return new Promise((resolve, reject) => {
        arregloUsuarios.forEach((valor, indice) => {
            let condicionBusqueda = valor.nombre === objetoNombreApellido.nombre && valor.apellido === objetoNombreApellido.apellido
            if (condicionBusqueda) {
                resolve({
                    info: `el usuario ${valor.nombre} ${valor.apellido} fue encontrado}`
                })
            }
        });
        reject({
            info: `el usuario ${objetoNombreApellido.nombre} ${objetoNombreApellido.apellido} No fue encontrado}`
        })

    })

}