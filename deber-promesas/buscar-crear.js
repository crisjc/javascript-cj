// buscar usuario y si no lo encuentra crearlo
module.exports = (arregloUsuarios, objetoNombreApellido) => {
    return new Promise((resolve,reject)=>{
        arregloUsuarios.forEach((valor, indice) => {
            let condicionBusqueda = valor.nombre === objetoNombreApellido.nombre && valor.apellido === objetoNombreApellido.apellido
            if (condicionBusqueda) {
                resolve({
                    info: `el usuario ${valor.nombre} ${valor.apellido} fue encontrado}`
                })
            }
        });
        reject({
            detalle: 'fue creado el usuario',
            indice:arregloUsuarios.push(objetoNombreApellido),
            info: arregloUsuarios[10]
        })

    })

}

