// crear usuario
module.exports = (arregloUsuarios, objetoNombreApellido) => {

    return new Promise((resolve,reject)=>{
        arregloUsuarios.forEach((valor, indice) => {
            let condicionBusqueda = valor.nombre === objetoNombreApellido.nombre && valor.apellido === objetoNombreApellido.apellido
            if (condicionBusqueda) {
                reject({
                    info: `el usuario ${valor.nombre} ${valor.apellido} ya existe}`
                })
            }
        });
        resolve({
            detalle: 'fue creado el usuario',
            indice:arregloUsuarios.push(objetoNombreApellido),
            info: arregloUsuarios[10]
        })



    })

}