const suma = require('./suma.js')
const resta = require('./resta.js')
const multiplicacion = require('./multiplicacion')


module.exports = {
    suma, resta, multiplicacion
}