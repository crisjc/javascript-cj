var menu = require('console-menu');

console.log('Bienvenidos');

console.log('Seleccione una opcion de menu');

const arregloUsuario = []



const registrarUsuario = function (datos) {
    console.log('Datos ', datos);
    arregloUsuario.push(datos);
    const hayUsuarios = arregloUsuario.length > 0

    if(hayUsuarios){
        ejecutarMenu(true)
    }else {
        ejecutarMenu()
    }
  
}

const logearUsuario = function (datos) {
    console.log('Datos ', datos);
    console.log(arregloUsuario);
    seleccionarUsuario(arregloUsuario)
}

const seleccionarUsuario = (usuarios)=>{
    const hayUsuarios = usuarios.length > 0

    if(hayUsuarios){
        ejecutarMenuUsuarios(usuarios)
    }else {
        ejecutarMenu()
    }
}

// permitir seleccionar un usuario con el menu si y solo si existen usuarios registrados listar todo slos usuarios registrados en el menu

const ejecutarMenu = (agregarOpcion) => {

    const menuSeleccion =[
        { hotkey: '1', title: 'Crear usuario', data: { nombre: 'Adrian' } },
        { hotkey: '2', title: 'Logear usuario', data: { nombre: 'Vicente' } }
    ]


    if(agregarOpcion){
        menuSeleccion[2]= { hotkey: '3', title: 'Listar usuarios', data: { nombre: 'Adrian' }}
    }


    return menu(menuSeleccion, {
            header: 'Menu',
            border: true,
        })
        .then(item => {
            switch (item.hotkey) {
                case '1':
                    console.log('Opcion 1');
                    registrarUsuario(item.data);
                    break;
                case '2':
                    console.log('Opcion 2');
                    logearUsuario(item.data);
                    break;
                case '3':
                    console.log('Opcion 3');
                    ejecutarMenuUsuarios(arregloUsuario)
                    break;
                default:
                    console.log('Opcion invalida');
            }
        });
}
const ejecutarMenuUsuarios = (usuarios) => {

    let usuarios2 = []
   usuarios.forEach((valor,indice) => {
       let objetoMenu={
        hotkey:indice,
        title:valor.nombre
       }
       usuarios2.push(objetoMenu)
   });

   
    console.log('usuarios2', usuarios2)
    return menu(usuarios2, {
            header: 'Menu',
            border: true,
        })
        .then(item => {
            switch (item.hotkey) {
                case '1':
                    console.log('Opcion 1');
                    registrarUsuario(item.data);
                    break;
                case '2':
                    console.log('Opcion 2');
                    logearUsuario(item.data);
                    break;
                default:
                    console.log('Opcion invalida');
            }
        });
}

ejecutarMenu();