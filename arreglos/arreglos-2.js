// operadores arreglos 
// quue todos cumplan la condicion
var arregloVectores = [1, 2, 3, 4, 5, 6, 7,]
var conEvery = arregloVectores.every(valor => {
    return typeof valor === 'number'
})
// que al menos uno cumpla la condicion
var consome = arregloVectores.some(valor => {
    return typeof valor === 'number'
})
// console.log(consome)


function suma(arregloVectores) {
    let suma = 0;
    arregloVectores.forEach(function (valor) {
        suma += valor

    });
    return suma
}
// console.log(suma(arregloVectores))

//operador reduce nos permite realizar la sumna de los elementos sin listar

var totalReduce = arregloVectores.reduce((acumulador, valor) => {
    return acumulador + valor
}, 0);
// console.log('valor total con reduce', totalReduce)


var arregloJson = [
    {
        dueño: 'juan',
        mascota: 'michi',
        sexo:'M'
    },
    {
        dueño: 'cristihan',
        mascota: 'chiqui',
        sexo:'M'
        

    },
    {
        dueño: 'carlos',
        mascota: 'boby',
        sexo:'M'
    },
    {
        dueño: 'maria',
        mascota: 'tom',
        sexo:'F'
    },
    {
        dueño: 'jose',
        mascota: 'lulu',
        sexo:'M'
    }];
    
var propietarioMasculino =arregloJson.reduce((acumulador, valor) => {
  
    var esMasculino = valor.sexo === 'M'
    if(esMasculino)
    {
        acumulador.push(valor)
    }
    return acumulador
},[])    

console.log(propietarioMasculino)