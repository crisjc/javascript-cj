var operacionesAritmeticas ={
    suma: (a,b)=>{
        return a+b
    },resta:(a,b)=>{
        return a-b
    },multiplicacion:(a,b)=>{
        return a*b
    },divicion:(a,b)=>{
        return a/b
    },modulo:(a,b)=>{
        return a%b
    },
}






 // operaciones entre dos valores number
 /* console.log(operacionesAritmeticas.suma(4,5))
console.log(operacionesAritmeticas.resta(4,5))
console.log(operacionesAritmeticas.multiplicacion(4,5))
console.log(operacionesAritmeticas.divicion(4,5))
console.log(operacionesAritmeticas.modulo(4,5)) */

 //operaciones entre un number y un undefined
/*  console.log(operacionesAritmeticas.suma(5))
console.log(operacionesAritmeticas.resta(5))
console.log(operacionesAritmeticas.multiplicacion(5))
console.log(operacionesAritmeticas.divicion(5))
console.log(operacionesAritmeticas.modulo(5)) */

/**
 * 
 */

//operaciones entre un number y un null
/* console.log(operacionesAritmeticas.suma(5,null))
console.log(operacionesAritmeticas.resta(5,null))
console.log(operacionesAritmeticas.multiplicacion(5,null))
console.log(operacionesAritmeticas.divicion(5,null))
console.log(operacionesAritmeticas.modulo(5,null)) */

//operaciones entre un number y un string
/* console.log(operacionesAritmeticas.suma(5,'a'))
console.log(operacionesAritmeticas.resta(5,'a'))
console.log(operacionesAritmeticas.multiplicacion(5,'a'))
console.log(operacionesAritmeticas.divicion(5,'a'))
console.log(operacionesAritmeticas.modulo(5,'a')) */

//operaciones entre un number y un boolean
/* console.log(operacionesAritmeticas.suma(5,true))
console.log(operacionesAritmeticas.resta(5,false))
console.log(operacionesAritmeticas.multiplicacion(5,true))
console.log(operacionesAritmeticas.divicion(5,true))
console.log(operacionesAritmeticas.modulo(5,true)) */

//operaciones entre dos valores indefinidos
/* console.log(operacionesAritmeticas.suma())
console.log(operacionesAritmeticas.resta())
console.log(operacionesAritmeticas.multiplicacion())
console.log(operacionesAritmeticas.divicion())
console.log(operacionesAritmeticas.modulo())*/


//operaciones entre un indefinido y un string
/* console.log(operacionesAritmeticas.suma('hh'))
console.log(operacionesAritmeticas.resta('hh'))
console.log(operacionesAritmeticas.multiplicacion('hh'))
console.log(operacionesAritmeticas.divicion('hh'))
console.log(operacionesAritmeticas.modulo('hh')) */

//operaciones entre un indefinido y un null
/* console.log(operacionesAritmeticas.suma(null))
console.log(operacionesAritmeticas.resta(null))
console.log(operacionesAritmeticas.multiplicacion(null))
console.log(operacionesAritmeticas.divicion(null))
console.log(operacionesAritmeticas.modulo(null)) */

//operaciones entre un indefinido y boolean
/* console.log(operacionesAritmeticas.suma(true))
console.log(operacionesAritmeticas.resta(false))
console.log(operacionesAritmeticas.multiplicacion(true))
console.log(operacionesAritmeticas.divicion(true))
console.log(operacionesAritmeticas.modulo(true)) */


//operaciones entre dos valores string
/* console.log(operacionesAritmeticas.suma('b','a'))
console.log(operacionesAritmeticas.resta('2','3'))
console.log(operacionesAritmeticas.multiplicacion('4','a'))
console.log(operacionesAritmeticas.divicion('b','a'))
console.log(operacionesAritmeticas.modulo('b','a')) */

//operaciones entre un valor string y null
/* console.log(operacionesAritmeticas.suma('b',null))
console.log(operacionesAritmeticas.resta('2',null))
console.log(operacionesAritmeticas.multiplicacion('4',null))
console.log(operacionesAritmeticas.divicion('b',null))
console.log(operacionesAritmeticas.modulo('b',null)) */

//operaciones entre un valor string y boolean
/* console.log(operacionesAritmeticas.suma('b',true))
console.log(operacionesAritmeticas.resta('2',true))
console.log(operacionesAritmeticas.multiplicacion('4',true))
console.log(operacionesAritmeticas.divicion('b',true))
console.log(operacionesAritmeticas.modulo('b',true)) */


//operaciones entre dos valores nulos 
/* console.log(operacionesAritmeticas.suma(null,null))
console.log(operacionesAritmeticas.resta(null,null))
console.log(operacionesAritmeticas.multiplicacion(null,null))
console.log(operacionesAritmeticas.divicion(null,null))
console.log(operacionesAritmeticas.modulo(null,null)) */

//operaciones entre un valor nulo y boolean
/* console.log(operacionesAritmeticas.suma(null,true))
console.log(operacionesAritmeticas.resta(null,true))
console.log(operacionesAritmeticas.multiplicacion(null,true))
console.log(operacionesAritmeticas.divicion(null,true))
console.log(operacionesAritmeticas.modulo(null,true))
 */

//operaciones entre dos valores del tipo boolean
 console.log(operacionesAritmeticas.suma(true,true))
console.log(operacionesAritmeticas.resta(false,true))
console.log(operacionesAritmeticas.multiplicacion(true,true))
console.log(operacionesAritmeticas.divicion(true,true))
console.log(operacionesAritmeticas.modulo(true,true))





