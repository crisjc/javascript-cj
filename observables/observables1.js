const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

const numeros$ =of(1,2,3,4,5,6,7,'12')
const respuesta = (respuesta)=>{
    console.log('respuesta',respuesta)
}

const errorObservable =(error)=>{
    console.log('error',error)

}

const cuandoFinaliza  =() =>{
    console.log('ya finaliza todo')
}
function sumaUno(numero){
    console.log('esta es la suma')
    return Number(numero)+2
}
function multiplicacion(numero){
    console.log('esta es la multiplicacion')
    return Number(numero)*2
}

function filtarPares(numero){
    return numero%2===0
}


function promesaCualquiera(numero){
    const callbacksPromesa =(resolve,reject)=>{
        resolve(numero)
        reject(numero)

    }
    return new Promise(callbacksPromesa)
}
promesaCualquiera(4)
.then (respuesta => {
    console.log('es el resolve')
})
.catch(respuesta => {
    console.log('reject')
}


// numeros$
// .pipe(
//     map(sumaUno),
//     map(multiplicacion),
//     filter(filtarPares)
// )

// .subscribe(respuesta,errorObservable,cuandoFinaliza)











