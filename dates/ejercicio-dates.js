const random = require('random-js');
const moment = require('moment');

const arregloUsuarios = [
    { nombre: 'juan' },
    { nombre: 'jose' },
    { nombre: 'manuel' },
    { nombre: 'carlos' },
    { nombre: 'isarael' },
    { nombre: 'alfonso' },
    { nombre: 'cristihan' },
    { nombre: 'gloria' },
    { nombre: 'selena' },
    { nombre: 'dayana' }
]
var nuevoArregloUsuarios = arregloUsuarios.map((valor) => {
    valor.fechaNacimiento =`${random.integer(1940, 2018)}-${random.integer(1,12)}-${random.integer(1,31)}`
    valor.edadUsuario= calcularEdad(valor.fechaNacimiento)
    return valor
})
function calcularEdad(fecha){

    let fechaActual =moment()
    return fechaActual.year() - moment(fecha).year()

}
const menoresEdad =nuevoArregloUsuarios.reduce((acumulador, valor) => {
  
    const esMenor = (valor.edadUsuario>=1 && valor.edadUsuario < 18)
    if(esMenor)
    {
        acumulador.push(valor)
    }
    return acumulador
},[]) 
const edadJovenes = nuevoArregloUsuarios.reduce((acumulador, valor) => {
  
    const esJoven = (valor.edadUsuario>=18 && valor.edadUsuario < 55)
    if(esJoven)
    {
        acumulador.push(valor)
    }
    return acumulador
},[])
const terceraEdad = nuevoArregloUsuarios.reduce((acumulador, valor) => {
  
    var terceraEdad = (valor.edadUsuario>=55 && valor.edadUsuario <= 78)
    if(terceraEdad)
    {
        acumulador.push(valor)
    }
    return acumulador
},[])
console.log('arreglo original \n',nuevoArregloUsuarios)
console.log('Menores de edad  \n',menoresEdad)
console.log('Edad Promedio \n',edadJovenes)
console.log('Tercera edad \n',terceraEdad)
